// Advance Query Operators
	// We want more flexible querying of data within MongoDB

// [SECTION] Comparison Query Operators

	// $gt - greater than
	// $gte - greater than equal
	/*
		Syntax: 
		db.collectionName.find({field: 
		})

	*/

	db.users.find(
	{
		age:{$gt: 21}
	})

	db.users.find(
	{
		age:{$gte: 21}
	})

	// $lt - less than
	// $lte - less than or equal

	db.users.find(
	{
		age: {$lt: 82}
	})

	db.users.find(
	{
		age: {$lte: 82}
	})

	// $ne - not equal
	db.users.find({age: {$ne:82}})

	// $in (something like OR)
	/*
		- Allows us to find documents with specific match criteria with one field
		using different values

		Syntax:
		db.collectionName.find($field:{$in: [valueA, valueB]})

	*/
	
	db.users.find(
	{
		lastName:{
			$in: ['Hawking', 'Doe']
		}
	})	

	db.users.find({
		courses: {
			$in: ["HTML", "React"]
		}
	})

	// $or (1 true = true) // multiple field criteria
	/*
		Syntax:
		db.collectionName.find({$or[{fieldA:valueA}, {fieldB:valueB}]})
	*/

	db.users.find(
		{
			$or: [
				{firstName: "Neil"},
				{age: 21}
			]	
		}
	);


	// $and (all should be satisfied)

	/*
		Syntax:
		db.collectionName.find({$and[{fieldA:valueA}, {fieldB:valueB}]})
	*/

	db.users.find(
		{
			$and: [
				{age: {$ne: 82}},
				{age: {$ne: 76}}
			]	
		}
	);

// [SECTION] Field Projection
// To help with readability of the values returned, we can include/exclude fileds from the response
/*
	Syntax:
	db.collectionName.find({criteria}, {fields you want to include, it should have a value of 1})
*/

// Inclusion (1)
db.users.find(
	{
		// criteria
		firstName: "Jane"	
	},
	{
		firstName:1,
		lastName:1,
		contact:1
	}
)

// Exclusion (0)
db.users.find(
	{
		// criteria
		firstName: "Jane"	
	},
	{
		
		contact: 0,
		department: 0
	}
)


// Supressing the ID field (not suggestable as it will be contradicting and make error)
db.users.find(
{
	// criteria
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	department: 0
}
)

// $regex (it should match anything in a text)

// Case Sensitive (Example)
db.users.find(
{
	firstName: {
		$regex: 'N'
	}
})


// Not case sensitive
db.users.find(
{
	firstName: {
		$regex: 'N', $options: '$i'
	}
})








